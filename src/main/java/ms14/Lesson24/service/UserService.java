package ms14.Lesson24.service;

import lombok.RequiredArgsConstructor;
import ms14.Lesson24.config.security.JwtService;
import ms14.Lesson24.dto.LoginResponseDto;
import ms14.Lesson24.dto.UserRegisterDto;
import ms14.Lesson24.model.Authority;
import ms14.Lesson24.model.User;
import ms14.Lesson24.model.UserAuthority;
import ms14.Lesson24.repository.AuthorityRepository;
import ms14.Lesson24.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static org.hibernate.boot.model.process.spi.MetadataBuildingProcess.build;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private final ModelMapper modelMapper;
    private final BCryptPasswordEncoder passwordEncoder;
    private final JwtService jwtService;


//    public ResponseEntity register(UserRegisterDto registerDto) {
//        if (userRepository.existsByUsername(registerDto.getUsername()) == true) {
//            return new ResponseEntity<>("Username is already taken", HttpStatus.BAD_REQUEST);
//        }
//
//        if (userRepository.existsByEmail(registerDto.getEmail()) == true) {
//            return new ResponseEntity<>("Email already in use", HttpStatus.BAD_REQUEST);
//        }
//        User user = new User(registerDto.getPassword(), registerDto.getUsername(), registerDto.getEmail());
//        user.setPassword(passwordEncoder.encode(registerDto.getPassword()));
//        userRepository.save(user);
//        return ResponseEntity.ok("SUCCESS");
//    }

    public LoginResponseDto register(UserRegisterDto registerDto) {
        Optional<User> user = userRepository.findByUsername(registerDto.getUsername());
        if (user.isPresent()) {
            throw new RuntimeException("Username present. Try different username");
        }
        Authority userAuthority = authorityRepository.findByAuthority(UserAuthority.USER).
                orElseThrow(() -> new RuntimeException("Authority not found"));

        if (!registerDto.getPassword().equals(registerDto.getRepeatPassword())) {
            throw new RuntimeException("Passwords did not match");
        }

        User userForSave = User.builder()
                .authorities(List.of(userAuthority))
                .password(passwordEncoder.encode(registerDto.getPassword()))
                .username(registerDto.getUsername())
                .build();

        userRepository.save(userForSave);

        return LoginResponseDto.builder()
                .jwt(jwtService.issueToken(userForSave))
                .build();
    }
}
