package ms14.Lesson24.controller;

import lombok.RequiredArgsConstructor;
import ms14.Lesson24.dto.LoginResponseDto;
import ms14.Lesson24.dto.UserRegisterDto;
import ms14.Lesson24.dto.UserResponseDto;
import ms14.Lesson24.model.User;
import ms14.Lesson24.service.UserDetailsServiceImpl;
import ms14.Lesson24.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/register")
@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserDetailsServiceImpl userDetailsService;
    private final UserService userService;


   @PostMapping()
    public ResponseEntity<LoginResponseDto> register(@RequestBody UserRegisterDto registerDto){
      return ResponseEntity.ok(userService.register(registerDto));
   }
}