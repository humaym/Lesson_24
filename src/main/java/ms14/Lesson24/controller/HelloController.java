package ms14.Lesson24.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ms14.Lesson24.config.security.SecurityService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1")
@Slf4j
@RequiredArgsConstructor

public class HelloController {

    private final SecurityService securityService;

    //public for everyone
    @GetMapping("/hello")

    public String getHello() {
        log.info("Current user details is {}", securityService.getCurrentJwtCredentials());
        return "Hello from Spring, everyone";
    }

    //user and admin can see this
    @GetMapping("/user")
    public String getUser() {
        return "Hello from Spring, USER";
    }

    //only admin
    @GetMapping("/admin")
    public String getAdmin() {
        return "Hello from Spring, ADMIN";
    }

    @GetMapping
    public String get() {
        return "Hello for GET";
    }

    @PostMapping
    public String post() {
        return "Hello for Post";
    }
}
