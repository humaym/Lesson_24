package ms14.Lesson24.repository;

import ms14.Lesson24.model.Authority;
//import ms14.Lesson24.model.UserAuthority;
import ms14.Lesson24.model.UserAuthority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {

    Optional<Authority> findByAuthority(UserAuthority userAuthority);
}
