package ms14.Lesson24.config;

import lombok.RequiredArgsConstructor;
import ms14.Lesson24.config.security.AuthFilterConfigurerAdapter;
import ms14.Lesson24.config.security.TokenAuthService;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.security.config.Customizer.withDefaults;

@EnableWebSecurity
@Configuration
@RequiredArgsConstructor
public class SecurityConfig {

    private final TokenAuthService tokenAuthService;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
//        http.authorizeHttpRequests(auth->auth.requestMatchers("v1/*").permitAll());
//        http.authorizeHttpRequests(auth -> auth.requestMatchers(GET, "/v1").permitAll());
//        http.authorizeHttpRequests(auth -> auth.requestMatchers(POST, "/v1").authenticated())
//        http.authorizeHttpRequests(auth -> auth.requestMatchers("/user").permitAll());
//        http.authorizeHttpRequests(auth -> auth.requestMatchers("/v1/user").hasAnyAuthority("USER", "ADMIN"));
//        http.authorizeHttpRequests(auth -> auth.requestMatchers("/v1/admin").hasAuthority("ADMIN"));
//        http.authorizeHttpRequests(auth -> auth.anyRequest().authenticated());
//        http.httpBasic(withDefaults());


        http.csrf(csrf -> csrf.disable());
        http.cors(cors -> cors.disable());
        http.sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS));

        http.authorizeHttpRequests(auth -> auth.requestMatchers("/register").permitAll());
        http.authorizeHttpRequests(auth -> auth.requestMatchers("/v1/hello").hasAuthority("USER"));
        http.authorizeHttpRequests(auth -> auth.anyRequest().authenticated());
        http.apply(new AuthFilterConfigurerAdapter(tokenAuthService));
        return http.build();
    }

//    @Bean
//    public InMemoryUserDetailsManager userDetailsService() {
//        UserDetails user = User.withDefaultPasswordEncoder()
//                .username("user")
//                .password("password")
//                .roles("USER")
//                .build();
//
//        UserDetails admin = User.withDefaultPasswordEncoder()
//                .username("admin")
//                .password("password")
//                .roles("ADMIN")
//                .build();
//
//        return new InMemoryUserDetailsManager(user, admin);
//    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
