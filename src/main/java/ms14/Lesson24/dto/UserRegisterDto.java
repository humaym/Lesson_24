package ms14.Lesson24.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserRegisterDto {
//    String email;
    String username;
    String password;
    String repeatPassword;
}
