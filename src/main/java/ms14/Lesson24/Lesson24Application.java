package ms14.Lesson24;

import lombok.RequiredArgsConstructor;
import ms14.Lesson24.model.Authority;
import ms14.Lesson24.model.User;
//import ms14.Lesson24.model.UserAuthority;
import ms14.Lesson24.model.UserAuthority;
import ms14.Lesson24.repository.AuthorityRepository;
import ms14.Lesson24.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

//import static ms14.Lesson24.model.UserAuthority.ADMIN;
//import static ms14.Lesson24.model.UserAuthority.USER;

@SpringBootApplication
@RequiredArgsConstructor
public class Lesson24Application implements CommandLineRunner {

    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(Lesson24Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

//        Authority user = Authority
//                .builder()
//                .authority(UserAuthority.USER)
//                .build();
//
//        Authority admin = Authority
//                .builder()
//                .authority(UserAuthority.ADMIN)
//                .build();
//
//        authorityRepository.save(user);
//        authorityRepository.save(admin);
//
//        User user1 = User.builder()
//                .username("Humay")
//                .password(bCryptPasswordEncoder.encode("12345"))
//                .authorities(List.of((Authority) Collections.singletonList(user)))
//                .build();
//
//
//        User admin1 = User.builder()
//                .username("Humay1")
//                .password(bCryptPasswordEncoder.encode("11111"))
//                .authorities(List.of((Authority) Collections.singletonList(admin)))
//                .build();
//
//        userRepository.save(user1);
//        userRepository.save(admin1);
    }
}
